/**
 * To be run `pnpm exp 2019`
 *
 * */
import fs from "fs";

function makeVariation(folderName, expId) {
    const parentFolder = `#${expId}/${folderName}`;
    fs.mkdirSync(parentFolder);

    fs.writeFileSync(
        `${parentFolder}/index.js`,
        `console.log('${expId} - ${folderName}........')`,
    );
    fs.writeFileSync(
        `${parentFolder}/style.css`,
        `#${folderName}_${expId} {
    }`,
    );
}

function main(expId) {
    fs.mkdirSync(`#${expId}`);

    makeVariation(`TC`, expId);
    makeVariation(`V1`, expId);
    makeVariation(`V2`, expId);
    makeVariation(`V3`, expId);

    fs.writeFileSync(
        `#${expId}/urls.json`,
        JSON.stringify(["https://angi.com", "https://angi.com/about"]),
    );
}

const folder = process.argv.slice(2);
main(folder);
