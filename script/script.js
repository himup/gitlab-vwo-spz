import fs from "fs";

const {
	FILES,
	VWO_API_KEY,
	VWO_ACCOUNT_ID,
	CI_MERGE_REQUEST_IID,
	CI_MERGE_REQUEST_SOURCE_BRANCH_NAME,
	CI_MERGE_REQUEST_TITLE,
	CI_MERGE_REQUEST_TARGET_BRANCH_NAME,
	CI_MERGE_REQUEST_PROJECT_ID,
} = process.env;

// ========= TESTING & DEBUGGING ONLY ==========
// console.log({
// 	env: {
// FILES,
// VWO_API_KEY,
// VWO_ACCOUNT_ID,
// CI_MERGE_REQUEST_IID,
// CI_MERGE_REQUEST_SOURCE_BRANCH_NAME,
// CI_MERGE_REQUEST_TITLE,
// CI_MERGE_REQUEST_TARGET_BRANCH_NAME,
// CI_MERGE_REQUEST_PROJECT_ID,
// 	},
// });

const VWO_BASE_URL = "https://app.vwo.com/api/v2";

const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

async function main(request) {
	const body = request;

	console.log({
		bodyAttributes: body.object_attributes,
	});

	const { id: projectId } = body.project;
	const {
		iid: mergeRequestIid,
		source_branch,
		target_branch,
		title,
	} = body.object_attributes;

	if (target_branch !== "main" && target_branch !== "master") {
		console.error("Not a merge request to main/master branch");
		return { message: "Not a merge request to main/master branch" };
	}

	console.log(
		"-------- Getting Control & Variation Files from Merge Request ---------",
	);

	const { urls, control, variations } =
		await getControlVariationFromMergeRequest();

	console.log(`--------- Creating VWO Campaign & Adding JS/CSS. Name of Campaign: ${title} ------------`);

	// ======= VWO Part starts here ========
	const res = await createVWOCampaign({
		name: title,
		urls,
		control,
		variations,
	});

	if (res.errors || res._errors) {
		console.error("Error creating VWO Campaign", res.errors || res._errors);
	} else {
		console.log("--------- VWO Campaign created successfully ---------");
	}

	return res;
}

/** ============ VWO ============== */
async function createVWOCampaign({ name, urls, control, variations }) {
	const existingCampaign = await getCampaignFromName(name);

	await sleep(1000);

	if (existingCampaign) {
		const res = await updateJSCSSVWOCampaign({
			campaign: existingCampaign,
			control,
			variations,
		});
		await startCampaign(existingCampaign.id);
		return res;
	}

	if (urls.length === 0) throw new Error("No URLs provided");

	const primaryUrl = new URL(urls[0]).origin;

	const percentSplit = 100 / (variations.length + 2);

	const reqBody = {
		name,
		type: "ab",
		urls: urls.map((u) => ({ type: "url", value: u })),
		primaryUrl,
		variations: [
			{
				name: "Control",
				isControl: true,
				isDisabled: false,
				percentSplit,
				editorData: { stack: [], createdBy: "code-base-editor" },
				isBase: true,
			},
			{
				name: "TC",
				isControl: false,
				isDisabled: false,
				percentSplit,
				isBase: true,
				editorData: getEditorDataFromJSCSS(control),
			},
			...variations.map((variation, i) => ({
				name: "V" + (i + 1),
				isControl: false,
				isDisabled: false,
				percentSplit,
				isBase: true,
				editorData: getEditorDataFromJSCSS(variation),
			})),
		],
		goals: [
			{
				name: "New goal",
				type: "visitPage",
				urls: urls.map((u) => ({ type: "url", value: u })),
			},
		],
	};

	console.log("----- Creating VWO Campaign ---------");

	// ----- TESTING & DEBUGGING ONLY --------
	// console.log(reqBody);

	const response = await fetch(
		`${VWO_BASE_URL}/accounts/${VWO_ACCOUNT_ID}/campaigns`,
		{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				token: VWO_API_KEY,
			},
			body: JSON.stringify(reqBody),
		},
	);

	// start campaign
	// await startCampaign(existingCampaign.id);

	return await response.json();
}

/** Update the campaign with the new code */
async function updateJSCSSVWOCampaign({ campaign, control, variations }) {
	const newVariations = campaign.variations.map((v) => {
		if (v.name.toLowerCase() === "tc") {
			v.editorData = getEditorDataFromJSCSS(control);
		} else if (v.name.toLowerCase().startsWith("v")) {
			const variation = variations.find((v2) => v2.name === v.name);
			if (variation) {
				v.editorData = getEditorDataFromJSCSS(variation);
			}
		}
		return v;
	});

	const body = { campaigns: { variations: newVariations } };

	console.log("----- Updating VWO Campaign ------");
	return await fetch(
		`${VWO_BASE_URL}/accounts/${VWO_ACCOUNT_ID}/campaigns/${campaign.id}`,
		{
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				token: VWO_API_KEY,
			},
			body: JSON.stringify(body),
		},
	).then((r) => r.json());
}

function getEditorDataFromJSCSS({ js, css }) {
	const editorData = {
		stack: [],
		createdBy: "code-base-editor",
	};

	if (js) {
		editorData.stack.push({
			op: {
				opName: "content",
				content: `<script type='text/javascript'>${js}\n</script>`,
				originalContent: js,
				opType: "js",
			},
			XPath: "HEAD",
			modified: true,
		});
	}
	if (css) {
		editorData.stack.push({
			op: {
				opName: "content",
				content: `<style type='text/css'>${css}\n</style>`,
				originalContent: css,
				opType: "css",
			},
			XPath: "HEAD",
			modified: true,
		});
	}
	return editorData;
}

async function getCampaignFromName(campaignName) {
	console.log("----- Getting ALL VWO Campaigns -------");
	const response = await fetch(
		`${VWO_BASE_URL}/accounts/${VWO_ACCOUNT_ID}/campaigns`,
		{
			method: "GET",
			headers: { "Content-Type": "application/json", token: VWO_API_KEY },
		},
	);

	const res = await response.json();
	const campaigns = res._data.partialCollection;

	const campaignId = campaigns.find((c) => c.name === campaignName)?.id;
	if (!campaignId) return undefined;

	await sleep(1000);

	console.log("----- Getting VWO Campaign -------");

	const campaign = await getCampaign(campaignId);
	return campaign;
}

/** Only allowed to Admins */
async function startCampaign(campaignId) {
	console.log("----- Starting VWO Campaign -------");
	return null;

	const response = await fetch(
		`${VWO_BASE_URL}/accounts/${VWO_ACCOUNT_ID}/campaigns/status`,
		{
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				token: VWO_API_KEY,
				body: JSON.stringify({ ids: [campaignId], status: "RUNNING" }),
			},
		},
	);

	return response.json();
}

async function getCampaign(campaignId) {
	const val = await fetch(
		`${VWO_BASE_URL}/accounts/${VWO_ACCOUNT_ID}/campaigns/${campaignId}`,
		{
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				token: VWO_API_KEY,
			},
		},
	).then((res) => res.json());

	return val._data;
}

async function getControlVariationFromMergeRequest() {
	const { jsFileContents, cssFileContents, jsonFileContents } =
		await getJSCSSFromMergeRequest();

	if (jsonFileContents.length === 0) throw new Error("No JSON files found");

	// --- Write to files ---
	// fsWriteJSCSSJSONToFiles({ mergeRequestIid, jsFileContents, cssFileContents, json: jsonFileContents[0].content });

	const urls = parseJSONToGetControlVariation(jsonFileContents[0].content);

	const variationsJS = jsFileContents.filter(
		(file) => file.filePath.toLowerCase().search(/v\d/g) !== -1,
	);
	const variationsCSS = cssFileContents.filter(
		(file) => file.filePath.toLowerCase().search(/v\d/g) !== -1,
	);

	const allIndexes = [...variationsJS, ...variationsCSS].reduce(
		(set, variationFile) => {
			const match = variationFile.filePath.toLowerCase().match(/v\d/g);
			if (match) set.add(parseInt(match[0].replace("v", "")));
			return set;
		},
		new Set(),
	);

	const controlJS = jsFileContents.find((file) =>
		file.filePath.toLowerCase().search("tc"),
	)?.content;
	const controlCSS = cssFileContents.find((file) =>
		file.filePath.toLowerCase().search("tc"),
	)?.content;

	return {
		urls,
		control: { js: controlJS, css: controlCSS },
		variations: Array.from(allIndexes).map((index) => ({
			name: `V${index}`,
			js: variationsJS.find((file) =>
				file.filePath.toLowerCase().includes(`v${index}`),
			)?.content,
			css: variationsCSS.find((file) =>
				file.filePath.toLowerCase().includes(`v${index}`),
			)?.content,
		})),
	};
}

function parseJSONToGetControlVariation(json) {
	const parsedJSON = JSON.parse(json);
	// return ["https://spiralyze.com/test"];
	return parsedJSON;
}

function fetchDiffsGitlab() {
	/** We get the value from the outside environment variable which takes care of getting the diffed files for us */
	const diff_files =
		FILES?.split("\n")
			.map((s) => s.trim())
			.filter(Boolean) || [];
	return diff_files;
}

/** @returns {{ filePath: string, content: string }} */
function getFileContentsFromGitlab(filePath) {
	return { filePath, content: fs.readFileSync(filePath).toString() };
}

async function getJSCSSFromMergeRequest() {
	const res = fetchDiffsGitlab();

	// ------ TESTING AND DEBUGGING ONLY --------
	// const allContents = res.map(getFileContentsFromGitlab);
	// console.log({ diff_files: res, allContents });

	const jsFiles = res.filter(
		(diff) => diff.endsWith(".js") && diff.match(/#\d+/g),
	);
	const cssFiles = res.filter(
		(diff) => diff.endsWith(".css") && diff.match(/#\d+/g),
	);
	const jsonFiles = res.filter(
		(diff) => diff.endsWith(".json") && diff.match(/#\d+/g),
	);

	const jsFileContents = jsFiles.map(getFileContentsFromGitlab);
	const cssFileContents = cssFiles.map(getFileContentsFromGitlab);
	const jsonFileContents = jsonFiles.map(getFileContentsFromGitlab);

	// ------ TESTING & DEBUGGING ONLY --------
	// console.log({
	// 	jsFileContents,
	// 	cssFileContents,
	// 	jsonFileContents,
	// });

	return {
		jsFileContents,
		cssFileContents,
		jsonFileContents,
	};
}

/**
 * Only for testing and seeing the results of the API.
 * */

/* eslint-disable @typescript-eslint/no-unused-vars */
function fsWriteJSCSSJSONToFiles({
	mergeRequestIid,
	jsFileContents,
	cssFileContents,
	json,
}) {
	const fs = require("fs");
	const d = new Date().toISOString().replaceAll(":", "-");
	if (!fs.existsSync(`tmp/merge-requests/${mergeRequestIid}`))
		fs.mkdirSync(`tmp/merge-requests/${mergeRequestIid}`);

	for (const file of jsFileContents)
		fs.writeFileSync(
			`tmp/merge-requests/${mergeRequestIid}/jsFiles-${file.filePath.replaceAll(
				"/",
				"_",
			)}-${d}.js`,
			file.content,
		);

	for (const file of cssFileContents)
		fs.writeFileSync(
			`tmp/merge-requests/${mergeRequestIid}/cssFiles-${file.filePath.replaceAll(
				"/",
				"_",
			)}-${d}.css`,
			file.content,
		);

	fs.writeFileSync(
		`tmp/merge-requests/${mergeRequestIid}/urls-${d}.json`,
		json,
	);
}

// main({
// 	project: {
// 		id: 51606842,
// 	},
// 	object_attributes: {
// 		iid: "18",
// 		source_branch: "stage",
// 		target_branch: "main",
// 		title: "added exp 2019 and modified script",
// 	},
// });

main({
	project: {
		id: CI_MERGE_REQUEST_PROJECT_ID,
	},
	object_attributes: {
		iid: CI_MERGE_REQUEST_IID,
		source_branch: CI_MERGE_REQUEST_SOURCE_BRANCH_NAME,
		target_branch: CI_MERGE_REQUEST_TARGET_BRANCH_NAME,
		title: CI_MERGE_REQUEST_TITLE,
	},
});
